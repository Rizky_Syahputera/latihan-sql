1. ( Buat Database )
create database myshop;

2. ( Buat Table Database )
use myshop;
( table users )
 create table users(
    -> id int(9) primary key auto_increment,
    -> name varchar(225),
    -> email varchar(225),
    -> password varchar(225)
    -> );
alter table users
    -> modify column name varchar(255) not null,
    -> modify column email varchar(255) not null,
    -> modify password varchar(255) not null; 

( table categories )
create table categories(
    -> id int (8) primary key auto_increment,
    -> name varchar(225) not null
    -> );
alter table categories
    -> modify column name varchar(255) not null; 

( table items )
create table item(
    -> id int (9) primary key auto_increment,
    -> name varchar(255) not null,
    -> description varchar(255) not null,
    -> price int (8) not null,
    -> stock int (5) not null,
    -> categories_id int (8) not null,
    -> foreign key(categories_id) references categories(id)
    -> );

3. ( Memasukan Data pada Table )
(users)
insert into users(name, email, password)
    -> values ("John Doe", "john@doe.com", "john123") , ("Jane Doe", "jane@doe.com", "jenita123");

(categories)
insert into categories(name)
    -> value ("gadget") , ("cloth") , ("men") , ("women") , ("branded");

(item)
insert into item(name, description, price, stock, categories_id) values
    -> ("sumsang b50" , "hape keren dari merek sumsang" , 4000000, 100 , 1),
    -> ("Uniklooh", "baju keren dari brand ternama" , 500000, 50 , 2),
    -> ("IMHO watch", "jam tangan anak yang jujur banget", 2000000, 10 , 1);

4. ( Mengambil Data dari Database )
a.   select id, name, email from users;
b-1. select * from item where price > 1000000;
b-2. select * from item where name like '%sang%';
c.   select item.name, item.description, item.price, item.stock, item.categories_id, categories.name from item inner join categories on item.categories_id = categories.id;
     select item.name, item.description, item.price, item.stock, item.categories_id, categories.name as kategori from item inner join categories on item.categories_id = categories.id;	

5. Mengubah Data dari Database
update item set price = 2500000 where id=1;
